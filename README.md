PNG TO PETSCII
==============

`PNG2PETSCII` is a proof of concept [PNG](http://www.libpng.org/pub/png/) to [PETSCII](https://www.c64-wiki.com/index.php/PETSCII) image converter.

VERSION
-------

Version 0.02-SNAPSHOT (2022-01-31)

INSTALLATION
------------

Add the following automatic export to your `~/.bash_profile`:

    export _JAVA_OPTIONS="-Xms1024m -Xmx2G -Xss256m"

In order to build and run an application JAR type the following:

    $ git clone https://pawelkrol@bitbucket.org/pawelkrol/scala-png2petscii.git
    $ cd scala-png2petscii/
    $ sbt clean update compile test package proguard:proguard

    $ java -Dfile.encoding=UTF8 -jar target/scala-2.13/proguard/png2petscii-0.02-SNAPSHOT.jar --help
    $ java -Dfile.encoding=UTF8 -jar target/scala-2.13/proguard/png2petscii-0.02-SNAPSHOT.jar --image image.png --background 0 --screen screen.prg --colors colors.prg

An example PETSCII image viewer can be compiled and run using:

    $ cd scala-png2petscii/viewer/
    $ vim viewer.src
    # Here you have to locate two labels: "screen" and "colors", and update paths to both included binaries
    $ make
    $ make run

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2016-2022 by Pawel Krol.

This library is free open source software; you can redistribute it and/or modify it under [the same terms](https://bitbucket.org/pawelkrol/scala-png2petscii/src/master/LICENSE.md) as Scala itself, either Scala version 2.13.0 or, at your option, any later version of Scala you may have available.

PLEASE NOTE THAT IT COMES WITHOUT A WARRANTY OF ANY KIND!
