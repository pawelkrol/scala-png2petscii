CHANGES
=======

Revision history for `PNG2PETSCII`, a proof of concept [PNG](http://www.libpng.org/pub/png/) to [PETSCII](https://www.c64-wiki.com/index.php/PETSCII) (also known as CBM ASCII, a character set used in Commodore 64) image converter.

0.02-SNAPSHOT (2022-01-31)
--------------------------

* Update [Scala](http://scala-lang.org/) library to version [2.13.8](https://github.com/scala/scala/releases)

0.01 (2016-05-19)
-----------------

* Initial version (enables conversion of PNG images into PETSCII format using a limited set of characters, and writing output picture data into screen and colour map files)
