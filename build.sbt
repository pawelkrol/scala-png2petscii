lazy val root = (project in file(".")).
  settings(
    fork in run := true,
    javaOptions += "-Xmx1G",
    javacOptions ++= Seq("-encoding", "UTF-8"),
    maxErrors := 1,
    name := "png2petscii",
    scalaVersion := "2.13.8",
    scalacOptions ++= Seq("-deprecation", "-explaintypes", "-feature", "-uniqid"),
    version := "0.02-SNAPSHOT"
  )

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.10",
  "com.github.scopt" %% "scopt" % "3.7.1",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
  "gov.nih.imagej" % "imagej" % "1.47",
  "org.c64.attitude" % "afterimage" % "0.08",
  "org.scalatest" %% "scalatest" % "3.0.9" % "test"
)

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "-" + module.revision + "." + artifact.extension
}

enablePlugins(SbtProguard)

proguardFilteredInputs in Proguard ++= ProguardOptions.noFilter((packageBin in Compile).value)

proguardInputs in Proguard := (dependencyClasspath in Compile).value.files

proguardOptions in Proguard += ProguardOptions.keepMain("org.c64.attitude.PNG2PETSCII.Application")

proguardOptions in Proguard += ProguardConf.PNG2PETSCII

proguardVersion in Proguard := "6.2.2"
