package org.c64.attitude.PNG2PETSCII

import java.io.File

import org.scalatest.FunSpec

import scala.io.Codec.ISO8859
import scala.io.Source

import Helper.TestUtil.{ buildEmptyImage, buildTempName, upscaleImagePlus }

class ConverterSpec extends FunSpec {

  private val screen = buildTempName(".prg").getCanonicalPath
  private val colors = buildTempName(".prg").getCanonicalPath

  private val arguments = Arguments(Some("/images/full-moon.jpg"), Some(screen), Some(colors))

  describe("downscale") {
    it("downscales image proportionally to width") {
      val converter = Converter(arguments)
      val scaledImage = converter.downscale(buildEmptyImage(300, 150))
      assert(scaledImage.getWidth == 80)
      assert(scaledImage.getHeight == 40)
    }

    it("downscales image proportionally to height") {
      val converter = Converter(arguments)
      val scaledImage = converter.downscale(buildEmptyImage(360, 300))
      assert(scaledImage.getWidth == 60)
      assert(scaledImage.getHeight == 50)
    }
  }

  describe("upscale") {
    it("upscales image by factor of 4") {
      val converter = Converter(arguments)
      val scaledImage = converter.upscale(buildEmptyImage(80, 50))
      assert(scaledImage.getWidth == 320)
      assert(scaledImage.getHeight == 200)
    }

    it("upscales image proportionally to width") {
      val converter = Converter(arguments)
      val scaledImage = converter.upscale(buildEmptyImage(80, 40))
      assert(scaledImage.getWidth == 320)
      assert(scaledImage.getHeight == 160)
    }

    it("upscales image proportionally to height") {
      val converter = Converter(arguments)
      val scaledImage = converter.upscale(buildEmptyImage(60, 50))
      assert(scaledImage.getWidth == 240)
      assert(scaledImage.getHeight == 200)
    }
  }

  describe("extend") {
    it("extends image to bitmap dimensions") {
      val converter = Converter(arguments)
      val extendedImage = converter.extend(buildEmptyImage(300, 180))
      assert(extendedImage.getWidth == 320)
      assert(extendedImage.getHeight == 200)
    }
  }

  describe("showPicture") {
    ignore("shows downscaled picture") {
      val converter = Converter(arguments)
      val picture = converter.downscale(converter.imagePlus)
      val upscaledPicture = upscaleImagePlus(picture, 16)
      upscaledPicture.show
    }

    ignore("shows upscaled picture") {
      val converter = Converter(arguments)
      converter.downscale(converter.imagePlus)
      val picture = converter.upscale(converter.imagePlus)
      val upscaledPicture = upscaleImagePlus(picture, 4)
      upscaledPicture.show
    }

    ignore("shows extended picture") {
      val converter = Converter(arguments)
      converter.downscale(converter.imagePlus)
      converter.upscale(converter.imagePlus)
      val picture = converter.extend(converter.imagePlus)
      val upscaledPicture = upscaleImagePlus(picture, 4)
      upscaledPicture.show
    }

    ignore("shows converted picture") {
      val converter = Converter(arguments)
      val picture = converter.createPicture
      converter.showPicture(picture)
    }
  }

  describe("convert") {
    it("converts PNG image into PETSCII format") {
      Converter(arguments).convert
      assert(Source.fromFile(screen)(ISO8859).toArray.head.toByte === 0x7e.toByte)
      assert(Source.fromFile(colors)(ISO8859).toArray.head.toByte === 0x0b.toByte)
      new File(screen).delete
      new File(colors).delete
    }
  }
}
