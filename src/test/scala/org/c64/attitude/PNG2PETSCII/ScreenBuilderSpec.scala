package org.c64.attitude.PNG2PETSCII

import org.c64.attitude.Afterimage.Mode.Data.{ Bitmap, Screen }
import org.c64.attitude.Afterimage.Mode.HiRes

import org.scalatest.{ BeforeAndAfter, FunSpec }

class ScreenBuilderSpec extends FunSpec with BeforeAndAfter {

  private var hiRes: HiRes = _

  before {
    var bitmapData = Array.fill(0x1f38){0x00.toByte} ++ Array.fill(0x04){0x0f.toByte} ++ Array.fill(0x04){0xff.toByte}
    var screenData = Array.fill(0x03e7){0x00.toByte} :+ 0x10.toByte

    hiRes = new HiRes(
      bitmap = new Bitmap(bitmapData, Bitmap.maxCols, Bitmap.maxRows),
      screen = Some(new Screen(screenData, Screen.maxCols, Screen.maxRows)),
      border = None
    )
    hiRes
  }

  describe("screen") {
    it("retrieves screen map in a text mode from a hires picture") {
      assert(ScreenBuilder(hiRes).screen.last === 0xfe.toByte)
    }
  }

  describe("colors") {
    it("retrieves screen map in a text mode from a hires picture") {
      assert(ScreenBuilder(hiRes).colors.last === 0x01.toByte)
    }
  }
}
