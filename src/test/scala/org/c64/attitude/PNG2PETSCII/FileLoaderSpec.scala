package org.c64.attitude.PNG2PETSCII

import org.scalatest.FunSpec

import Helper.TestUtil.buildTempName
import Util.resourcePath

class FileLoaderSpec extends FunSpec {

  private def fileLoader(name: String) = FileLoader(name)

  describe("when provided name to non-existing image") {
    it("fails to initialize properly") {
      intercept[IllegalArgumentException] { FileLoader(buildTempName(".png").getCanonicalPath) }
    }
  }

  describe("fullPath") {
    describe("when provided name to internal resource image") {
      it("returns path to internal resource image") {
        val name = "/images/full-moon.jpg"
        assert(FileLoader(name).fullPath === resourcePath("/images/full-moon.jpg"))
      }
    }

    describe("when provided name to file system image") {
      it("returns path to file system image") {
        val name = System.getProperty("user.dir") + "/src/main/resources/images/full-moon.jpg"
        assert(FileLoader(name).fullPath === name)
      }
    }
  }
}
