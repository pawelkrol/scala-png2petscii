package org.c64.attitude.PNG2PETSCII
package Helper

import ij.process.ImageProcessor
import ij.process.ImageProcessor.NONE
import ij.ImagePlus

import java.io.File
import javax.imageio.ImageIO.{ read, write }

import scala.util.Random

object TestUtil {

  private val tempDir = new File(System.getProperty("java.io.tmpdir"))

  def buildEmptyImage(width: Int, height: Int): ImagePlus = {
    val stream = getClass().getResourceAsStream("/images/full-moon.jpg")
    val image = read(stream)

    val imagePlus = new ImagePlus("", image)
    imagePlus.setTitle("Test Image")

    val imageProcessor = imagePlus.getProcessor
    imageProcessor.setInterpolationMethod(NONE)

    val scaledImageProcessor = imageProcessor.resize(width, height)
    imagePlus.setProcessor(imagePlus.getTitle, scaledImageProcessor)

    imagePlus
  }

  def buildTempFile(extension: String): File = {
    val file = File.createTempFile("png2petscii-", extension, tempDir)
    file.deleteOnExit
    file
  }

  def buildTempName(extension: String): File = {
    val file = new File(tempDir.getCanonicalPath + "/" + Random.alphanumeric.take(16).mkString("") + extension)
    if (file.exists)
      buildTempName(extension)
    else
      file
  }

  def saveImageToFile(imagePlus: ImagePlus, fileName: String): Unit = {
    write(imagePlus.getProcessor.getBufferedImage, "png", new File(fileName))
  }

  def upscaleImagePlus(imagePlus: ImagePlus, scaleFactor: Int): ImagePlus = {
    val ip = imagePlus.getProcessor
    val width = ip.getWidth * scaleFactor
    val height = ip.getHeight * scaleFactor
    ip.setInterpolationMethod(NONE)
    val upscaledImage = ip.resize(width, height).getBufferedImage
    new ImagePlus("", upscaledImage)
  }
}
