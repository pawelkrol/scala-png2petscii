package org.c64.attitude.PNG2PETSCII

import org.scalatest.FunSpec

class CharSetSpec extends FunSpec {

  private val charset = CharSet()

  describe("find") {
    it("finds a character identified by 4x4 square fills within a charset") {
      assert(charset.find((0, 0, 0, 1)) === 0x6c.toByte)
    }
  }
}
