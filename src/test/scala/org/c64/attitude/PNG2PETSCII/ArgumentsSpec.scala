package org.c64.attitude.PNG2PETSCII

import org.scalatest.FunSpec
import org.scalatest.Matchers._

import Helper.TestUtil.{ buildTempFile, buildTempName }

class ArgumentsSpec extends FunSpec {

  private var image: Option[String] = _
  private var screen: Option[String] = _
  private var colors: Option[String] = _
  private var bckgrd: Option[Int] = _

  private def arguments = Arguments(image, screen, colors, bckgrd)

  private def defaultImage = Some("/images/full-moon.jpg")
  private def defaultScreen = Some(buildTempName(".prg").getCanonicalPath)
  private def defaultColors = Some(buildTempName(".prg").getCanonicalPath)
  private def defaultBckgrd = Some(0x00)

  describe("validate") {
    it("all arguments should be validated") {
      image = defaultImage
      screen = defaultScreen
      colors = defaultColors
      bckgrd = defaultBckgrd
      noException should be thrownBy { arguments.validate }
    }

    it("source image file should be specified") {
      image = None
      screen = defaultScreen
      colors = defaultColors
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Missing source image filename\n"
    }

    it("source image file should exist") {
      image = Some(buildTempName(".png").getCanonicalPath)
      screen = defaultScreen
      colors = defaultColors
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Source image file does not exist: %s\n".format(image.get)
    }

    it("target screen file should be specified") {
      image = defaultImage
      screen = None
      colors = defaultColors
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Missing target screen filename\n"
    }

    it("target screen file should not exist") {
      image = defaultImage
      screen = Some(buildTempFile(".prg").getCanonicalPath)
      colors = defaultColors
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Target screen file already exists: %s\n".format(screen.get)
    }

    it("target colors file should be specified") {
      image = defaultImage
      screen = defaultScreen
      colors = None
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Missing target colors filename\n"
    }

    it("target colors file should not exist") {
      image = defaultImage
      screen = defaultScreen
      colors = Some(buildTempFile(".prg").getCanonicalPath)
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Target colors file already exists: %s\n".format(colors.get)
    }

    it("source image file should be valid image") {
      image = Some(buildTempFile(".png").getCanonicalPath)
      screen = defaultScreen
      colors = defaultColors
      bckgrd = defaultBckgrd
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Unrecognized image file type: %s\n".format(image.get)
    }

    it("does not require background colour to be specified") {
      image = defaultImage
      screen = defaultScreen
      colors = defaultColors
      bckgrd = None
      noException should be thrownBy { arguments.validate }
    }

    it("background colour should not be negative") {
      image = defaultImage
      screen = defaultScreen
      colors = defaultColors
      bckgrd = Some(-1)
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Invalid background colour: %s\n".format(bckgrd.get)
    }

    it("background colour should not be greater than $0f") {
      image = defaultImage
      screen = defaultScreen
      colors = defaultColors
      bckgrd = Some(0x10)
      val thrown = intercept[IllegalArgumentException] { arguments.validate }
      thrown.getMessage shouldBe "Error: Invalid background colour: %s\n".format(bckgrd.get)
    }
  }
}
