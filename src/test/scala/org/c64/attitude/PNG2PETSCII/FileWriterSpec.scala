package org.c64.attitude.PNG2PETSCII

import java.io.File

import org.scalatest.FunSpec

import scala.io.Codec.ISO8859
import scala.io.Source

import Helper.TestUtil.{ buildTempFile, buildTempName }

class FileWriterSpec extends FunSpec {

  private def fileWriter(name: String) = FileWriter(name)

  describe("when provided name to an already existing file") {
    it("fails to initialize properly") {
      val fileName = buildTempFile(".prg").getCanonicalPath
      val thrown = intercept[IllegalArgumentException] { FileWriter(fileName) }
      assert(thrown.getMessage === "Error: Unable to write data into: %s\n".format(fileName))
    }
  }

  describe("when provided name to a non-existing file") {
    it("saves data to a file on disk") {
      val fileName = buildTempName(".prg").getCanonicalPath
      val data = Array[Byte](0x10, 0x01, 0x17, 0x05, 0x0c, 0x20, 0x0b, 0x12, 0x00)
      FileWriter(fileName).saveData(data)
      assert(data === Source.fromFile(fileName)(ISO8859).toArray)
      new File(fileName).delete
    }
  }
}
