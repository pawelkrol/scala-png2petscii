package org.c64.attitude.PNG2PETSCII

import org.scalatest.FunSpec

class CharResolverSpec extends FunSpec {

  describe("resolve") {
    it("resolves recognised character into a screen code") {
      assert(CharResolver(1, 1, 1, 0).resolve === 0xec.toByte)
    }
  }
}
