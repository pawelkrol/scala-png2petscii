package org.c64.attitude.PNG2PETSCII

import org.scalatest.FunSpec

import Util.{ fileExists, resourcePath }
import Helper.TestUtil.{ buildTempFile, buildTempName }

class UtilSpec extends FunSpec {

  describe("fileExists") {
    it("returns true if resource exists") {
      val resource = "/images/full-moon.jpg"
      assert(fileExists(resource) === true)
    }

    it("returns true if file exists") {
      val filename = buildTempFile(".txt").getCanonicalPath
      assert(fileExists(filename) === true)
    }

    it("returns false if neither file nor resource exist") {
      val filename = buildTempName(".txt").getCanonicalPath
      assert(fileExists(filename) === false)
    }
  }

  describe("resourcePath") {
    it("resolves resource name into full path") {
      assert(resourcePath("/images/full-moon.jpg") === System.getProperty("user.dir") + "/target/scala-2.13/classes/images/full-moon.jpg")
    }
  }
}

