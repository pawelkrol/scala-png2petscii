package org.c64.attitude.PNG2PETSCII

import java.io.{ File, PrintStream }

class FileWriter(val fullPath: String) {

  def saveData(bytes: Array[Byte]): Unit = {
    val writer = new PrintStream(fullPath)
    writer.write(bytes)
    writer.close
  }
}

object FileWriter {

  def apply(name: String) =
    if ((new File(name)).exists)
      throw new IllegalArgumentException("Error: Unable to write data into: %s\n".format(name))
    else
      new FileWriter(name)
}
