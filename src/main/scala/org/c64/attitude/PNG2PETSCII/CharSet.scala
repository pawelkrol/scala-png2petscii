package org.c64.attitude.PNG2PETSCII

class CharSet(charset: Map[Tuple4[Int, Int, Int, Int], Byte]) {

  def find(char: Tuple4[Int, Int, Int, Int]) = charset.find(_._1 == char).get._2
}

object CharSet {

  private val charset = Map[Tuple4[Int, Int, Int, Int], Int](
    (0, 0, 0, 0) -> 0x20,
    (1, 0, 0, 0) -> 0x7e,
    (0, 1, 0, 0) -> 0x7c,
    (0, 0, 1, 0) -> 0x7b,
    (0, 0, 0, 1) -> 0x6c,
    (1, 1, 0, 0) -> 0xe2,
    (0, 0, 1, 1) -> 0x62,
    (1, 0, 1, 0) -> 0x61,
    (0, 1, 0, 1) -> 0xe1,
    (1, 0, 0, 1) -> 0x7f,
    (0, 1, 1, 0) -> 0xff,
    (0, 1, 1, 1) -> 0xfe,
    (1, 0, 1, 1) -> 0xfc,
    (1, 1, 0, 1) -> 0xfb,
    (1, 1, 1, 0) -> 0xec,
    (1, 1, 1, 1) -> 0xa0
  ).map({ case (font, code) => font -> code.toByte })

  def apply() = new CharSet(charset)
}
