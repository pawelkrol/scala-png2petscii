package org.c64.attitude.PNG2PETSCII

import Util.fileExists

case class Arguments(
  image: Option[String] = None,
  screen: Option[String] = None,
  colors: Option[String] = None,
  bckgrd: Option[Int] = Some(0x00)
) {

  def validate: Unit = {
    image match {
      case Some(filename) => {
        if (fileExists(filename))
          try {
            FileLoader(filename).openImage
          }
          catch {
            case e: RuntimeException =>
              throw new IllegalArgumentException("Error: Unrecognized image file type: %s\n".format(filename))
          }
        else
          throw new IllegalArgumentException("Error: Source image file does not exist: %s\n".format(filename))
      }
      case None =>
        throw new IllegalArgumentException("Error: Missing source image filename\n")
    }

    screen match {
      case Some(filename) =>
        if (fileExists(filename))
          throw new IllegalArgumentException("Error: Target screen file already exists: %s\n".format(filename))
      case None =>
        throw new IllegalArgumentException("Error: Missing target screen filename\n")
    }

    colors match {
      case Some(filename) =>
        if (fileExists(filename))
          throw new IllegalArgumentException("Error: Target colors file already exists: %s\n".format(filename))
      case None =>
        throw new IllegalArgumentException("Error: Missing target colors filename\n")
    }

    bckgrd match {
      case Some(colour) =>
        if (colour < 0x00 || colour > 0x0f)
          throw new IllegalArgumentException("Error: Invalid background colour: %s\n".format(colour))
      case None =>
    }
  }
}
