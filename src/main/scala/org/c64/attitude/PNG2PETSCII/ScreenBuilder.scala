package org.c64.attitude.PNG2PETSCII

import java.util.NoSuchElementException

import org.c64.attitude.Afterimage.Mode.{ CBM, HiRes }

class ScreenBuilder(picture: CBM) {

  private val hiRes = picture.asInstanceOf[HiRes]

  private val data = (0 until 25).flatMap(y => (0 until 40).map(x => fourSquares(x, y)))

  private def squareCheck(x: Int, y: Int, colour: Int) = if (hiRes.pixel(x, y) == colour) 0x01 else 0x00

  private def fourSquares(x: Int, y: Int) = {
    val offsetX = x * 8
    val offsetY = y * 8

    val screenColour = try {
      hiRes.screen.get(x, y)
    }
    catch {
      case error: Exception => {
        throw new NoSuchElementException("Screen data outside of definite boundaries requested: [%d,%d]".format(x, y))
      }
    }
    val backgroundColour = screenColour & 0x0f
    val paintColour = (screenColour & 0xf0) >> 4

    val lt = squareCheck(offsetX + 0, offsetY + 0, paintColour)
    val rt = squareCheck(offsetX + 4, offsetY + 0, paintColour)
    val lb = squareCheck(offsetX + 0, offsetY + 4, paintColour)
    val rb = squareCheck(offsetX + 4, offsetY + 4, paintColour)

    (CharResolver(lt, rt, lb, rb).resolve, paintColour.toByte)
  }

  val screen = data.map(_._1)

  val colors = data.map(_._2)
}

object ScreenBuilder {

  def apply(picture: CBM) = new ScreenBuilder(picture)
}
