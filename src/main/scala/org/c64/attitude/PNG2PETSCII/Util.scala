package org.c64.attitude.PNG2PETSCII

import java.io.File

object Util {

  def resourcePath(name: String) = getClass.getResource(name).toString.replace("file:", "")

  def fileExists(filename: String) =
    (new File(filename)).exists || (Option(getClass.getResource(filename)) match {
      case Some(resource) => true
      case None => false
    })
}
