package org.c64.attitude.PNG2PETSCII

import com.typesafe.scalalogging.StrictLogging
import scopt.OptionParser

object Application extends StrictLogging {

  private val appVersion = "0.02-SNAPSHOT"

  private val parser = new OptionParser[Arguments]("java -Dfile.encoding=UTF8 -jar png2petscii-%s.jar".format(appVersion)) {
    head("png2petscii", appVersion)
    help("help") text("prints out this usage text")
    opt[String]("image")
      .optional()
      .action { (value: String, option: Arguments) => option.copy(image = Some(value)) }
      .text("defines source image filename (e.g., \"image.png\")")
    opt[String]("screen")
      .optional()
      .action { (value: String, option: Arguments) => option.copy(screen = Some(value)) }
      .text("defines target screen filename (e.g., \"screen.prg\")")
    opt[String]("colors")
      .optional()
      .action { (value: String, option: Arguments) => option.copy(colors = Some(value)) }
      .text("defines target colours filename (e.g., \"colors.prg\")")
    opt[Int]("background")
      .optional()
      .action { (value: Int, option: Arguments) => option.copy(bckgrd = Some(value)) }
      .text("defines optional background colour (e.g., \"15\", must be between \"0\" and \"15\", defaults to \"0\")")
    // TODO: Add an option to save a preview image of a rendered screen picture to a PNG file
  }

  def main(args: Array[String]) = {
    println("\nPNG2PETSCII Image Converter %s (2019-07-13)\nCopyright (C) 2016-2019 Pawel Krol (DJ Gruby/Protovision/TRIAD)\n".format(appVersion))

    parser.parse(args, Arguments()) match {
      case Some(arguments) => {
        try {
          arguments.validate
        } catch {
          case e: Exception => {
            println(e.getMessage)
            System.exit(1)
          }
        }
        runWith(arguments)
      }
      case None =>
        // arguments are invalid, usage message will have been displayed
    }
  }

  def runWith(arguments: Arguments): Unit = {
    logger.debug("Conversion process initialized")
    Converter(arguments).convert
    logger.debug("Conversion process completed")
  }
}
