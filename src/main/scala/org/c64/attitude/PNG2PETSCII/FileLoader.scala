package org.c64.attitude.PNG2PETSCII

import ij.ImagePlus
import ij.process.ImageConverter

import scala.io.Codec.ISO8859
import scala.io.Source

import Util.resourcePath

class FileLoader(val fullPath: String) {

  def openImage = {
    val imagePlus = new ImagePlus(fullPath)
    val imageConverter = new ImageConverter(imagePlus)
    imageConverter.convertToRGB()
    imagePlus
  }
}

object FileLoader {

  private def fromFile(name: String) =
    if (!Source.fromFile(name)(ISO8859).hasNext)
      throw new IllegalArgumentException("Error: Unable to load file from: %s\n".format(name))
    else
      new FileLoader(name)

  private def fromResource(name: String) =
    if (!Source.fromInputStream(getClass.getResourceAsStream(name))(ISO8859).hasNext)
      throw new IllegalArgumentException("Error: Unable to load resource from: %s\n".format(name))
    else
      new FileLoader(resourcePath(name))

  def apply(name: String) =
    try {
      fromFile(name)
    }
    catch {
      case _: Throwable =>
        try {
          fromResource(name)
        }
        catch {
          case _: Throwable => throw new IllegalArgumentException("Error: Unable to load data from: %s\n".format(name))
        }
    }
}
