package org.c64.attitude.PNG2PETSCII

sealed trait FileType

object ResourceFile extends FileType

object SystemFile extends FileType
