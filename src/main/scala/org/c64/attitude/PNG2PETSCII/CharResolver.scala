package org.c64.attitude.PNG2PETSCII

class CharResolver(lt: Int, lb: Int, rt: Int, rb: Int) {

  val charset = CharResolver.charset

  def resolve = charset.find((lt, lb, rt, rb))
}

object CharResolver {

  val charset = CharSet()

  def apply(lt: Int, lb: Int, rt: Int, rb: Int) = new CharResolver(lt, lb, rt, rb)
}
