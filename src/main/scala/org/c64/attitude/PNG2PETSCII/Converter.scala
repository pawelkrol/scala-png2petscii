package org.c64.attitude.PNG2PETSCII

import ij.plugin.CanvasResizer
import ij.process.ImageProcessor.{ BICUBIC, BILINEAR, NONE }
import ij.ImagePlus
import ij.IJ

import org.c64.attitude.Afterimage.Colour.Palette
import org.c64.attitude.Afterimage.File.Import.HiRes
import org.c64.attitude.Afterimage.Mode.CBM
import org.c64.attitude.Afterimage.View.Image

import scala.math.Ordering.Double.TotalOrdering

class Converter(arguments: Arguments) {

  private val screenWidth = 40 * 2
  private val screenHeight = 25 * 2

  private val bitmapWidth = screenWidth * 4
  private val bitmapHeight = screenHeight * 4

  val imagePlus = FileLoader(arguments.image.get).openImage

  // TODO: Find the most common C64 colour in the image and use it as a background colour during conversion process instead of a hard-coded $00
  val backgroundColour = arguments.bckgrd.getOrElse(0x00).toByte

  def scale(imagePlus: ImagePlus, desiredWidth: Int, desiredHeight: Int, interpolationMethod: Int) = {
    val width = imagePlus.getWidth
    val height = imagePlus.getHeight

    val scaleX = desiredWidth.toDouble / width
    val scaleY = desiredHeight.toDouble / height

    val scaleFactor = Array(scaleX, scaleY).min(TotalOrdering)

    val imageProcessor = imagePlus.getProcessor
    imageProcessor.setInterpolationMethod(interpolationMethod)

    val targetWidth = width * scaleFactor
    val targetHeight = height * scaleFactor

    val scaledImageProcessor = imageProcessor.resize(targetWidth.toInt, targetHeight.toInt)
    imagePlus.setProcessor(imagePlus.getTitle, scaledImageProcessor)

    imagePlus
  }

  def downscale(imagePlus: ImagePlus) = {
    scale(imagePlus, screenWidth, screenHeight, BICUBIC)
  }

  def upscale(imagePlus: ImagePlus) = {
    scale(imagePlus, bitmapWidth, bitmapHeight, NONE)
  }

  def extend(imagePlus: ImagePlus) = {
    val resizer = new CanvasResizer
    val imageProcessor = imagePlus.getProcessor
    val colour = Palette("default")(backgroundColour)
    IJ.setBackgroundColor(colour.red, colour.green, colour.blue)
    val extendedImage = resizer.expandImage(imageProcessor, bitmapWidth, bitmapHeight, 0, 0).getBufferedImage
    new ImagePlus("", extendedImage)
  }

  def createPicture = {
    // TODO: Image optimisations can be done here (sharpen edges, reduce number of colors, etc.)
    downscale(imagePlus)
    upscale(imagePlus)
    val fullImage = extend(imagePlus)
    HiRes(Some(backgroundColour)).convert(fullImage)
  }

  def showPicture(picture: CBM): Unit = {
    Image(picture, Palette("default")).show(2)
  }

  def convert: Unit = {
    val picture = createPicture
    val builder = ScreenBuilder(picture)
    val screenBytes = builder.screen.toArray
    val colorsBytes = builder.colors.toArray
    FileWriter(arguments.screen.get).saveData(screenBytes)
    FileWriter(arguments.colors.get).saveData(colorsBytes)
  }
}

object Converter {

  def apply(arguments: Arguments) = new Converter(arguments)
}
